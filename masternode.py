#!/usr/bin/env python


def getInfo():
    print("Wrong repo, please use https://gitlab.com/minerscore/masternodes instead")


if __name__ == '__main__':
    getInfo()